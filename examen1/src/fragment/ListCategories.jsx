import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { fetchCategories} from '../hooks/Conexion';
import axios from 'axios';
import Image from 'react-bootstrap/Image';
function ListCategories() {
  const [data, setData] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);

  useEffect(() => {
    const getData = async () => {
      
      try {
        const response = await fetchCategories();
        console.log(response.data);
        setData(response);
      } catch (error) {
        console.log("Error de informacion");
        console.error(error);
      }
    };
    getData();
   
  }, []);
  
  return (
    <div>
    <h1>Categorias</h1>
    <table>
      <thead>
        <tr>
          <th>Nombre</th>
          <th>Imagen</th>
          
        </tr>
      </thead>
      <tbody>
        {data.map(item => (
          <tr key={item.id}>
            <td>{item.name}</td>
            <td><Image src={item.image} rounded/></td>
          </tr>
        ))}
      </tbody>
    </table>
  </div>
  );
}

export default ListCategories;