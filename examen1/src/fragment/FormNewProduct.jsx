import React, { useState } from 'react';
import axios from 'axios';

function FormNewProduct() {
  const [formData, setFormData] = useState({
    title: "",
    price: "",
    description: "",
    images: "https://picsum.photos/640/640?r=2530",
    categoryId:"1"
  });

  const handleInputChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post('https://api.escuelajs.co/api/v1/products/', formData);
      console.log(response.data);
    } catch (error) {
      console.error(error);
    }
  };
  
  return (
    <div>
      <h1>Crear libro</h1>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Título:</label>
          <input type="text" name="title" value={formData.title} onChange={handleInputChange} />
        </div>
        <div>
          <label>Precio:</label>
          <input type="number" name="price" value={formData.price} onChange={handleInputChange} />
        </div>
        <div>
          <label>Descripcion:</label>
          <input type="text" name="description" value={formData.description} onChange={handleInputChange} />
        </div>
        <button type="submit">Enviar</button>
      </form>
    </div>
  );
}

export default FormNewProduct;
