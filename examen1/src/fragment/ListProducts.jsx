import React, { useState, useEffect } from 'react';
import { Button, Modal } from 'react-bootstrap';
import {fetchProducts} from '../hooks/Conexion';
import axios from 'axios';
import Image from 'react-bootstrap/Image';
function ListProducts() {
  const [data, setData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [selectedBook, setSelectedBook] = useState(null);
  const handleModify = (book) => {
    setSelectedBook(book);
    setShowModal(true);
  };
  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();
    try {
      await axios.put(`https://api.escuelajs.co/api/v1/products/${selectedBook.id}`, selectedBook);
      // Lógica adicional después de enviar los datos modificados correctamente
      setShowModal(false);
    } catch (error) {
      console.error(error);
      // Manejo de errores
    }
    setShowModal(false);
  };

  useEffect(() => {
    const getData = async () => {
      
      try {
        const response = await fetchProducts();
        console.log(response.data);
        setData(response);
      } catch (error) {
        console.log("Error de informacion");
        console.error(error);
      }
    };
    getData();
   
  }, []);
  
  return (
    <div>
    <h1>Categorias</h1>
    <table>
      <thead>
        <tr>
          <th>Titulo</th>
          <th>Precio</th>
          <th>Descripcion</th>
          <th>imagen</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        {data.map(item => (
          <tr key={item.id}>
            <td>{item.title}</td>
            <td>{item.price}</td>
            <td>{item.description}</td>
            <td><Image src={item.images} rounded/></td>
            <td>
              <button variant="warning" onClick={() => handleModify(item)}>Modificar</button>{' '}
            </td>
          </tr>
        ))}
      </tbody>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Modificar Producto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {selectedBook && (
            <form onSubmit={handleFormSubmit}>
              <div>
                <label>Título:</label>
                <input
                  type="text"
                  value={selectedBook.title}
                  onChange={(e) =>
                    setSelectedBook({ ...selectedBook, title: e.target.value })
                  }
                />
              </div>
              <div>
                <label>Precio:</label>
                <input
                  type="number"
                  value={selectedBook.price}
                  onChange={(e) =>
                    setSelectedBook({ ...selectedBook, price: e.target.value })
                  }
                />
              </div>
              <button type="submit">Guardar cambios</button>
            </form>
          )}
        </Modal.Body>
      </Modal>
    </table>
    
  </div>
  );
}

export default ListProducts;