import ListCategories from './fragment/ListCategories';
import MenuNavbar from './fragment/MenuNavbar';
import './App.css';
import {Navigate, Route, Routes, useLocation} from 'react-router-dom';
import ListProducts from './fragment/ListProducts';
import FormNewProduct from './fragment/FormNewProduct';
function App() {
  return (
    <div className="App">
      <MenuNavbar/>
      <Routes>
        <Route path='/categories' element={<ListCategories/>}/>
        <Route path='/products' element={<ListProducts/>}/>
        <Route path='/newproduct' element={<FormNewProduct/>}/>
      </Routes>
    </div>
  );
}

export default App;
