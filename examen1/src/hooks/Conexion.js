import axios from 'axios'
export const fetchCategories = async () => {
    try {
      const response = await axios.get('https://api.escuelajs.co/api/v1/categories');
      console.log(response.data);
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
};
export const fetchProducts = async () => {
    try {
      const response = await axios.get('https://api.escuelajs.co/api/v1/products');
      console.log(response.data);
      return response.data;
    } catch (error) {
      console.error(error);
      throw error;
    }
};
